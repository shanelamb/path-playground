var commonConfig = require('./webpack.common.config');

module.exports = {
    entry: './app',
    output: {
        path: './dist/bin',
        filename: 'app.bundle.js'
    },
    devtool: "source-map",
    resolve: {
        extensions: commonConfig.resolveExtensions
    },
    module: {
        loaders: commonConfig.moduleLoaders
    }
};

