module.exports = {
    moduleLoaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel?presets=es2015'
        },
        {
            test: /\.ts$/,
            exclude: /node_modules/,
            loader: 'ts-loader'
        }
    ],
    resolveExtensions: ['', '.ts', '.js']
};
