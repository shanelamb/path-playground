import { createAction } from 'redux-actions';

export const ADD_NODE = 'ADD_NODE';
export const REMOVE_NODE = 'REMOVE_NODE';
export const UPDATE_NODE_POSITION = 'UPDATE_NODE_POSITION';

export const addNode = createAction<symbol>(
    ADD_NODE,
);

export const removeNode = createAction<symbol>(
    REMOVE_NODE,
);

export type NodePositionUpdateParams = {
    id: symbol;
    x: number;
    y: number;
}

export const updateNodePosition = createAction<NodePositionUpdateParams>(
    UPDATE_NODE_POSITION,
);