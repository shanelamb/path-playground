export type Connection = {
    startNodeId: symbol;
    endNodeId: symbol;
    visited: number;
};

export type Node = {
    id: symbol;
    x: number;
    y: number;
};

export type Graph = {
    nodes: Node[];
    connections: Connection[];
    markers: {
        currentNodeId: symbol;
        startNodeId: symbol;
        endNodeId: symbol;
    }
};

export type State = Graph;