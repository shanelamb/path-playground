import { handleActions, Action } from 'redux-actions';
import { State } from './model'
import { ADD_NODE, REMOVE_NODE, NodePositionUpdateParams, UPDATE_NODE_POSITION } from './actions'

const initialState: State = {
    nodes: [],
    connections: [],
    markers: {
        currentNodeId: null,
        endNodeId: null,
        startNodeId: null
    }
};

export const graphReducer = handleActions<State, any>({
    [ADD_NODE]: (state: State, action: Action<symbol>) => {
        return {
            nodes: [
                {
                    id: action.payload,
                    x: 400,
                    y: 250
                },
                ...state.nodes
            ],
            connections: state.connections,
            markers: state.markers
        };
    },
    [REMOVE_NODE]: (state: State, action: Action<symbol>) => {
        return {
            nodes: state.nodes.filter(n => n.id !== action.payload),
            connections: state.connections,
            markers: state.markers
        }
    },
    [UPDATE_NODE_POSITION]: (state: State, action: Action<NodePositionUpdateParams>) => {
        let existing = state.nodes.find(n => n.id === action.payload.id);
        return {
            nodes: state.nodes.filter(n => n.id !== action.payload.id).concat(
                Object.assign({}, existing, action.payload)
            ),
            connections: state.connections,
            markers: state.markers
        }
    }
}, initialState);
