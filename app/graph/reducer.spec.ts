import { graphReducer } from './reducer';
import { createStore, Store } from 'redux';
import { addNode, removeNode } from './actions';
import { Graph } from './model';

describe('graph reducer', () => {
    var store: Store<Graph>;

    beforeEach(() => store = createStore(graphReducer));

    it('adds node', () => {
        let id = Symbol();
        store.dispatch(addNode(id));
        var state = store.getState();
        expect(state.nodes.length).toBe(1);
        expect(state.nodes[0].id).toBe(id);
    });

    it('removes node', () => {
        // first add node
        let id = Symbol();
        store.dispatch(addNode(id));

        //then remove
        store.dispatch(removeNode(id));

        var state = store.getState();
        expect(state.nodes.length).toBe(0);
    });
});
