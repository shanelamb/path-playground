import { Graph } from '../graph/model';
import { Store } from 'redux';
import IEvent = fabric.IEvent;
import IObject = fabric.IObject;
import { updateNodePosition } from '../graph/actions';

export class Renderer {
    constructor(private store: Store<Graph>, private canvas: fabric.ICanvas) {
        store.subscribe(() => this.onGraphUpdate(store.getState()));
        canvas.on('object:modified', (event: IEvent) => this.onObjectModified(event.target))
    }

    onGraphUpdate(graph: Graph) {
        this.canvas.clear();

        graph.nodes.forEach(node => {
            this.canvas.add(new fabric.Circle({
                data: node.id,
                left: node.x,
                top: node.y,
                strokeWidth: 5,
                radius: 10,
                fill: '#123',
                stroke: '#666',
                hasBorders: false,
                hasControls: false
            }));
        });

        this.canvas.renderAll();
    }

    onObjectModified(object: IObject) {
        let width = this.canvas.getWidth() - 30;
        let height = this.canvas.getHeight() - 30;

        if (object.left > width) object.left = width;
        if (object.left < 0) object.left = 0;
        if (object.top > height) object.top = height;
        if (object.top < 0) object.top = 0;

        this.store.dispatch(updateNodePosition({
            id: object.data,
            x: object.left,
            y: object.top
        }));
    }
}
