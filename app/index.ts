/// <reference path="../node_modules/@types/fabric/index.d.ts" />
/// <reference path="../node_modules/@types/jasmine/index.d.ts" />

import 'fabric';
import { createStore } from 'redux';
import { graphReducer } from './graph/reducer';
import { addNode } from './graph/actions';
import { Renderer } from './renderer/renderer';

var store = createStore(graphReducer);

store.subscribe(() => {
    console.log(store);
});

window.onload = () => {
    let canvas = new fabric.Canvas(document.querySelector('canvas'));
    canvas.setWidth(800);
    canvas.setHeight(500);
    let renderer = new Renderer(store, canvas);

    document.getElementById("add-node").addEventListener('click', () => {
        store.dispatch(addNode(Symbol()));
    });
};
